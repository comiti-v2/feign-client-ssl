package fr.derrieux.feign.clientpaysurf;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import feign.Headers;

/**
 * Bean permettant d'emmetre des requêtes sur l'api de paysurf
 * Ajouter dans l'annotation @Headers la liste des en-têtes génériques
 */
@FeignClient(
    name = "paysurf" , 
    url = "${paysurf.api.base-url}",
    configuration = PaysurfFeignConfig.class)
@RequestMapping(headers = {
    "x-api-key=${paysurf.api.headers.x-api-key}",
    "ps-ip-address=${paysurf.api.headers.ps-ip-address}"
})
public interface PaysurfClient {

    @GetMapping(
        value="/api/${paysurf.api.dev.partner}/accounts")
    public String getAccounts();
    
}
