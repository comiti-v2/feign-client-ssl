package fr.derrieux.feign.clientpaysurf;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@EnableFeignClients
@RestController
public class ClientPaysurfApplication {

	@Autowired
	PaysurfClient paysurf;


	public static void main(String[] args) {
		SpringApplication.run(ClientPaysurfApplication.class, args);

	}

	@GetMapping(value = "/")
	public String getPaysurf() throws IOException {
		return new String(this.paysurf.getAccounts());
	}
	
}
