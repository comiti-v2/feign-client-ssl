package fr.derrieux.feign.clientpaysurf;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Base64.Decoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.io.FileUtils;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import feign.Client;

/**
 * Classe de configuration du Feignclient pour le SSL 
 * 
 * Cette classe corrige le comportement de l'Objet HostnameVerifier qui retourne false a chaque vérifications du Hotsname du client (Erreur de Handshake)
 * On profite de la configuration pour gerer à "la main" l'envoie des certificats
 */
public class PaysurfFeignConfig {
    // A utiliser a la place d'un System.out
    private Logger log = Logger.getLogger("PaysurfFeignConfig");

    @Value("${paysurf.cert-file}")
    private File clientp12File;

    @Value("${paysurf.password}")
    private String clientP12Pass = "";

    /**
     * C'est a partir de ce bean que spring va instancier le feignClient
     * On active le SSL et on s'assure que la verification du hostname retourne vrai (todo : implémenter la vérification selon le contexte)
     * @return
     * @throws Exception
     */
    @Bean
    public Client feignClient() throws Exception {        
        SSLSocketFactory sslSocketFactory = this.gSocketFactory();
        return new Client.Default(sslSocketFactory, new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });
    }

    /**
     * Ajout des clés au contexte ssl de la socket afin de pouvoir communiquer
     * en https
     * 
     * on décode le contenu du fichier qui contient le certificat (from base64)
     * qu'on injecte dans un fichier (temporaire) afin de le passer au contexte.
     * 
     * @return
     * @throws Exception
     */
    public SSLSocketFactory gSocketFactory() throws Exception {
        Decoder decoder = java.util.Base64.getDecoder();
        byte[] p12 = decoder.decode(FileUtils.readFileToString(clientp12File));
        File p12File = new File("clientCer.p12");
        FileUtils.writeByteArrayToFile(p12File, p12);
        
        SSLContext sslContexts = SSLContexts
            .custom()
            .loadKeyMaterial(p12File, clientP12Pass.toCharArray(), clientP12Pass.toCharArray())
            .build();
        return sslContexts.getSocketFactory();
    
    }
}
